/// Adds extesnions to String
extension ExtensionOnString on String {
  /// Converts string to title case.
  ///
  /// If the string is already in title case, this method returns `this`.
  /// ```dart
  /// 'ABC'.toTitleCase(); // 'Abc'
  /// 'abc'.toTitleCase(); // 'Abc'
  /// 'Abc'.toTitleCase(); // 'Abc'
  /// 'abc abc abc'.toTitleCase(); // 'Abc Abc Abc'
  /// ```
  String toTitleCase() {
    final splits = split(' ');
    for (var i = 0; i < splits.length; i++) {
      splits[i] =
          // ignore: lines_longer_than_80_chars
          '${splits[i][0].toUpperCase()}${splits[i].substring(1).toLowerCase()}';
    }
    return splits.join(' ');
  }
}

/// Extensions on Nullable String
extension ExtensionOnNullableString on String? {
  /// Return a bool if the string is null or empty
  bool get isNullOrEmpty => this == null || this!.isEmpty;
}
