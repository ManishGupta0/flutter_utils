import 'package:flutter/material.dart';

/// Adds Extensions to Widget
extension ExtensionOnMaterial on Widget {
  /// Wraps widget with [ColoredBox] with given [color]
  Widget withColor(Color color) {
    return ColoredBox(
      color: color,
      child: this,
    );
  }

  /// Draws border around widget for layout debug
  Widget showOutlines([Color color = const Color(0xFF000000)]) {
    return Container(
      decoration: BoxDecoration(
        border: Border.all(color: color),
      ),
      child: this,
    );
  }
}
